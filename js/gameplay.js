import util.setProperty as setProperty;

var onSyncUpdate;

function pluginSend(evt, params) {
  NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent &&
    NATIVE.plugins.sendEvent("GamePlayPlugin", evt, JSON.stringify(params || {}));
}

function pluginOn(evt, next) {
  NATIVE && NATIVE.events && NATIVE.events.registerHandler &&
    NATIVE.events.registerHandler(evt, next);
}

function invokeCallbacks(list, clear) {
  // Pop off the first two arguments and keep the rest
  var args = Array.prototype.slice.call(arguments);
  args.shift();
  args.shift();

  // For each callback,
  for (var ii = 0; ii < list.length; ++ii) {
    var next = list[ii];

    // If callback was actually specified,
    if (next) {
      // Run it
      next.apply(null, args);
    }
  }

  // If asked to clear the list too,
  if (clear) {
    list.length = 0;
  }
}

var Gameplay = Class(function () {
  //list of defined callback function
  var loginCB = [];
  var signedInCB = [];
  var roomConnectedCB = [];
  var isMasterCB = [];
  var mapReceivedCB = [];
  var allAckReceivedCB = [];
  var startGameCB = [];
  var moveReceivedCB = [];
  var playerLeaveCB = [];
  var userCancelCB = [];
  var errorCB = [];

  this.init = function(opts) {
    logger.log("{gameplay} Registering for events on startup");
    setProperty(this, "onSyncUpdate", {
      set: function(f) {
        //logger.log("Am seting it");
        // If a callback is being set,
        if (typeof f === "function") {
          onSyncUpdate = f;
        } else {
          onSyncUpdate = null;
        }
      },
      get: function() {
        //logger.log("Am getting it");
        return onSyncUpdate;
      }
    });

    //definition of how manage message from Android
    pluginOn("gameplayLogin", function(evt) {
      logger.log("{gameplay} State updated:", evt.state);

      invokeCallbacks(loginCB, true, evt.state === "open", evt);
    });

    pluginOn("gameplaySignedIn", function(evt) {
      logger.log("{gameplay} State updated:", evt.state);

      invokeCallbacks(signedInCB, true, evt.state === "true", evt);
    });

    pluginOn("gameplayRoomConnected", function(evt) {
      logger.log("{gameplay} RoomConnected:", evt.state);

      invokeCallbacks(roomConnectedCB, false, evt.state, evt);
    });

    pluginOn("gameplayIsMaster", function(evt) {
      logger.log("{gameplay} is master:", evt.state);

      invokeCallbacks(isMasterCB, true, evt.state === "true", evt);
    });

    pluginOn("gameplayMapReceived", function(evt) {
      logger.log("{gameplay} map received:", evt.state);

      invokeCallbacks(mapReceivedCB, false, evt.state, evt);
    });

    pluginOn("gameplayAllAckReceived", function(evt) {
      logger.log("{gameplay} all ack received:", evt.state);

      invokeCallbacks(allAckReceivedCB, false, evt.state, evt);
    });

    pluginOn("gameplayStartGame", function(evt) {
      logger.log("{gameplay} start game:", evt.state);

      invokeCallbacks(startGameCB, false, evt.state, evt);
    });

    pluginOn("gameplayMoveDone", function(evt) {
      logger.log("{gameplay} move done:", evt.state);

      invokeCallbacks(moveReceivedCB, false, evt.state, evt);
    });

    pluginOn("gameplayPlayerLeave", function(evt) {
      logger.log("{gameplay} move done:", evt.state);

      invokeCallbacks(playerLeaveCB, false, evt.state, evt);
    });

    pluginOn("gameplayCancel", function(evt) {
      logger.log("{gameplay} user cancel", evt.state);

      invokeCallbacks(userCancelCB, false, evt.state, evt);
    });

    pluginOn("gameplayError", function(evt) {
      logger.log("{gameplay} GP error:", evt.state);

      invokeCallbacks(errorCB, false, evt.state, evt);
    });
    //
  };

  /** SETS OF FUNCTION USED TO MANAGE AND DEFINE CB **/
  this.setRoomConnectedCallback = function(callback) {
    logger.log("{gameplay} setRoomConnectedCallback");
    roomConnectedCB.push(callback)
  };

  this.setReceivedMapCallback = function(callback) {
    logger.log("{gameplay} setReceivedMapCallBack");
    mapReceivedCB.push(callback);
  };

  this.setAllAckReceivedCallback = function(callback) {
    logger.log("{gameplay} setAllAckReceivedCallback");
    allAckReceivedCB.push(callback);
  };

  this.setStartGameCallback = function(callback) {
    logger.log("{gameplay} setAllAckReceivedCallback");
    startGameCB.push(callback);
  };

  this.setMoveReceivedCallback = function(callback) {
    logger.log("{gameplay} setMoveReceivedCallback");
    moveReceivedCB.push(callback);
  };

  this.setOnPlayerLeaveCallback = function(callback) {
    logger.log("{gameplay} setOnPlayerLeftCallback");
    playerLeaveCB.push(callback);
  };

  this.setOnUserCancelCallback = function(callback) {
    logger.log("{gameplay} setOnUserCancelCallback");
    userCancelCB.push(callback);
  };

  this.setOnErrorCallback = function(callback) {
    logger.log("{gameplay} setOnErrorCallback");
    errorCB.push(callback);
  };
  /** END OF CB DEFINITION **/

  this.sendAchievement = function(achievementID, percentSolved) {
    logger.log("{gameplay} Sending of achievement");

    var param = {"achievementID":achievementID,"percentSolved":percentSolved};

    pluginSend("sendAchievement",param);
  };

  this.sendScore = function(leaderBoardID, score) {
    logger.log("{gameplay} Sending of Score to leaderboard");

    var param = {"leaderBoardID":leaderBoardID,"score":score};

    pluginSend("sendScore",param);
  };

  this.setNumber = function(name, val) {
    return;
  };

  this.initSync = function(param_name) {
    return;
  };

  this.logout = function() {
    logger.log("{gameplay} Logging Out a user");
    pluginSend("signOut");
  };

  this.login = function(next) {
    logger.log("{gameplay} Logging in a user");
    loginCB.push(next);
    pluginSend("beginUserInitiatedSignIn");
  };

  this.showLeaderBoard = function() {
    logger.log("{gameplay} Showing Leaderboard");
    pluginSend("showLeaderBoard");
  };

  this.showAchievements = function() {
    logger.log("{gameplay} Showing Achievements");
    pluginSend("showAchievements");
  };

  this.isSignedIn = function(callback) {
    logger.log("{gameplay} Showing Achievements");
    signedInCB.push(callback);
    pluginSend("isSignedIn");
  };
  
  /* 
   * NB. The callback roomConnectedCB is invoked when ready to play (all players are connected to the room)
   */
  this.invitePlayers = function() {
    logger.log("{gameplay} invitePlayers called");
    pluginSend("invitePlayers");
  };
  
  /*
   * Returns if this device is the game master or not 
   */
  this.isMaster = function(callback) {
    logger.log("{gameplay} isMaster");
    isMasterCB.push(callback);
    pluginSend("isMaster");
  };

  this.sendMap = function(jsonMap){
    logger.log("{gameplay} sending map");
    pluginSend("sendMap",jsonMap);
  };

  this.sendAck = function() {
    logger.log("{gameplay} sendAck");
    pluginSend("sendAck");
  };

  this.sendStartGame = function() {
    logger.log("{gameplay} sendStartGame");
    pluginSend("sendStartGame");
  };

  this.leaveGame = function() {
    logger.log("{gameplay} leaveGame");
    pluginSend("leaveGame");
  }

  this.sendMove = function(data) {
    logger.log("{gameplay} sendMove");
    pluginSend("sendMove",data);
  };
});

exports = new Gameplay();
